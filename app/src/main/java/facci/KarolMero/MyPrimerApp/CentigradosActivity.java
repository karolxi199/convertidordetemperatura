package facci.KarolMero.MyPrimerApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class CentigradosActivity extends AppCompatActivity {
    TextView Conversionn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centigrados);

        Conversionn = (TextView)findViewById(R.id.LBLResultadoUno);
        Double Fahrenheitt = Double.valueOf(getIntent().getStringExtra("fahrenheit"));

        double Ress = (Fahrenheitt - 32)/ 1.8 ;
        Conversionn.setText(getString(R.string.Respuesta) +" " + Ress+ getString(R.string.grados));
    }
}
