package facci.KarolMero.MyPrimerApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FahrenheitActivity extends AppCompatActivity {

    private TextView Conversion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fahrenheit);
        Conversion= (TextView)findViewById(R.id.LBLResultadoDos);
        Double Fahrenheitt = Double.valueOf(getIntent().getStringExtra("centigrados"));

        Double Result = (Fahrenheitt * 1.8) + 32;

        Conversion.setText(getString(R.string.Respuesta) +" " + Result + getString(R.string.gradoss));
    }
}
