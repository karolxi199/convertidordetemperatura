package facci.KarolMero.MyPrimerApp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText Grados;
    private Button Centigrados, Fahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Grados = (EditText)findViewById(R.id.TXTDatos);
        Centigrados=(Button)findViewById(R.id.BTNUno);
        Fahrenheit =(Button)findViewById(R.id.BTDos);

        Centigrados.setOnClickListener(this);
        Fahrenheit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.BTNUno:
                if (Grados.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getString(R.string.Toast), Toast.LENGTH_LONG).show();
                    Grados.requestFocus();
                }else{
                    String Fahrenheitt = Grados.getText().toString();
                    Intent intent = new Intent(MainActivity.this, CentigradosActivity.class);
                    intent.putExtra("fahrenheit", Fahrenheitt);
                    startActivity(intent);
                    Grados.setText("");
                }
                break;

            case R.id.BTDos:
                if (Grados.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, getString(R.string.Toast), Toast.LENGTH_LONG).show();
                    Grados.requestFocus();
                }else{
                    String Centigrades = Grados.getText().toString();
                    Intent intentt = new Intent(MainActivity.this, FahrenheitActivity.class);
                    intentt.putExtra("centigrados", Centigrades);
                    startActivity(intentt);
                    Grados.setText("");
                }
                break;
        }
    }
}
